import { AfterViewInit, Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit{
  title = 'chart-demo';
  public selectedNodes: any = [];


  ds = {
    id: '1',
    name: 'Mark',
    title: 'general manager',
    className: 'employee-card',
    profilepicture: '',

    children: [
      {
        id: '2',
        name: 'Ross',
        title: 'department manager',
        className: 'direct employee-card',
        profilepicture: '',
        isSelected: false
      },
      {
        id: '3',
        name: 'Quint',
        title: 'department manager',
        className: 'direct employee-card',
        profilepicture: '',
        isSelected: false,
        children: [
          {
            id: '4',
            name: 'Ram',
            title: 'senior engineer',
            className: 'secondary employee-card',
            profilepicture: '',
            isSelected: false
          },
          {
            id: '5',
            name: 'Mukesh',
            title: 'senior engineer',
            className: 'secondary employee-card',
            profilepicture: '',
            isSelected: false,
            children: [
              {
                id: '6',
                name: 'John',
                title: 'engineer',
                className: 'collobration employee-card',
                profilepicture: '',
                isSelected: false,
              },
              {
                id: '7',
                name: 'Dan Dan',
                title: 'engineer',
                className: 'collobration employee-card',
                profilepicture: '',
                isSelected: false,
              },
              {
                id: '8',
                name: 'Andrew',
                title: 'engineer',
                className: 'collobration employee-card',
                profilepicture: '',
                isSelected: false,
              },
              {
                id: '9',
                name: 'Alex',
                title: 'engineer',
                className: 'collobration employee-card',
                profilepicture: '',
                isSelected: false,
              },
              {
                id: '10',
                name: 'John S.',
                title: 'engineer',
                className: 'collobration employee-card',
                profilepicture: '',
                isSelected: false,
              },
              {
                id: '11',
                name: 'Ranjan',
                title: 'engineer',
                className: 'collobration employee-card',
                profilepicture: '',
                isSelected: false,
              },
            ],
          },
          {
            id: '12',
            name: 'Arjun',
            title: 'senior engineer',
            className: 'secondary employee-card',
            profilepicture: '',
            isSelected: false,
          },
          {
            id: '13',
            name: 'Parth',
            title: 'senior engineer',
            className: 'secondary employee-card',
            profilepicture: '',
            isSelected: false,
          },

          {
            id: '14',
            name: 'Meet',
            title: 'senior engineer',
            className: 'secondary employee-card',
            profilepicture: '',
            isSelected: false,
          },
          {
            id: '15',
            name: 'Shrey',
            title: 'senior engineer',
            className: 'secondary employee-card',
            profilepicture: '',
            isSelected: false,
          },
        ],
      },
      {
        id: '16',
        name: 'Rahul',
        title: 'department manager',
        className: 'direct employee-card',
        profilepicture: '',
        isSelected: false
      },
      {
        id: '17',
        name: 'Sachin',
        title: 'department manager',
        className: 'direct employee-card',
        profilepicture: '',
        isSelected: false
      },
      {
        id: '18',
        name: 'Alpesh',
        title: 'department manager',
        className: 'direct employee-card',
        profilepicture: '',
        isSelected: false
      },


    ],
  };


  constructor() {}

  ngOnInit() {

    // tslint:disable-next-line: prefer-for-of




  }

  ngAfterViewInit() {
    // Add class in parent node for setting up pseudo element design
    $('.secondary').parent().parent().parent().addClass('secondaryParent');
    console.log('class',  $('.secondary').parent().parent());

    $('.collobration').parent().parent().parent().addClass('collobrateParent');


  }

  test(nodeData: any, event: any) {
    $(document).removeClass('oc-is-selected')

    // Check if object is in array or not
    const isCheck = this.selectedNodes.includes(nodeData);
    console.log('is check', isCheck);
    if (!isCheck) {
        // push the object in array
        nodeData.isSelected = true;
        this.selectedNodes.push(nodeData);
    } else {
        // remove the object from array
        nodeData.isSelected = false;
        this.selectedNodes = this.selectedNodes.filter((el) => {
          return el.id !== nodeData.id;
        })
    }
    console.log('this selected node', this.selectedNodes);
  }
}
